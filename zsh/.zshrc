# Setup PATH
PATH=$PATH:~/Library/Python/3.7/bin
PATH=$PATH:~/.bin

# Completition
fpath=(~/.zsh/completion $fpath)
fpath=(/usr/local/share/zsh-completions $fpath)
autoload -Uz compinit && compinit -i

# Setup History
HISTSIZE=5000
SAVEHIST=5000
HISTFILE=~/.zsh_history
setopt extended_history
setopt append_history
setopt hist_ignore_dups
setopt hist_expire_dups_first
setopt hist_reduce_blanks
setopt hist_verify

# Better mv
autoload zmv

[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

transfer() {
    curl --progress-bar --upload-file "$1" https://transfer.sh/$(basename $1) | tee /dev/null;
}

# ZSH plugins
source ~/.zsh_plugins.sh

# Options for zsh-autosuggestions
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=white'

# Options for zsh-history-substring-search
# HISTORY_SUBSTRING_SEARCH_FUZZY=true

# Node Version Manager
export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

# Ruby Version Manager
export PATH=$PATH:$HOME/.rbenv/bin
eval "$(rbenv init -)"

# Aliases
alias transfer=transfer
alias ls='ls -G'
alias cp='rsync -avz'

# Keyboard shortcuts
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

bindkey '^ ' autosuggest-accept

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

