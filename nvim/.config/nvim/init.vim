filetype plugin on

" Plugins
call plug#begin('~/.local/share/vmin/plugged')

" Themes
" Plug 'morhetz/gruvbox'
" Plug 'dracula/vim', { 'as': 'dracula' }
" Plug 'altercation/vim-colors-solarized'
Plug 'morhetz/gruvbox'

" Lightline
Plug 'itchyny/lightline.vim'
Plug 'shinchu/lightline-gruvbox.vim'

" Airline
" Plug 'vim-airline/vim-airline'
" Plug 'vim-airline/vim-airline-themes'

" Indentation
Plug 'Yggdroot/indentLine' " TODO: configure

" Code completion
Plug 'ervandew/supertab' " TODO: configure

" Fancy start screen
Plug 'mhinz/vim-startify'

" Git stuff
Plug 'airblade/vim-gitgutter' " TODO: configure
Plug 'tpope/vim-fugitive' " TODO: configure

" File management
Plug 'scrooloose/nerdtree'
autocmd StdinReadPre * let s:std_in=1
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
map <C-n> :NERDTreeToggle<CR>

" Parenthess, brackets etc.
Plug 'tpope/vim-surround'

" Comments TODO: Figure our why it's not working :C
Plug 'scrooloose/nerdcommenter'
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 0
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

" Use deoplete
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
let g:deoplete#enable_at_startup = 1

" Linting:
" Engine
Plug 'w0rp/ale'
let g:ale_sign_column_always = 1
let g:ale_completion_enabled = 0
let g:ale_sign_error = '↑'
let g:ale_sign_warning = '→'
let g:ale_elixir_elixir_ls_release = '/Users/blazeroot/Projects/elixir-ls/rel'
let g:ale_elixir_elixir_ls_config = {'elixirLS': {'dialyzerEnabled': v:false}}
" Easymotion and friends
Plug 'easymotion/vim-easymotion'
Plug 'haya14busa/vim-easyoperator-line'
Plug 'haya14busa/vim-easyoperator-phrase'

" Language specific:
" Markdown:
Plug 'plasticboy/vim-markdown'

" Elixir:
Plug 'elixir-editors/vim-elixir'
Plug 'slashmili/alchemist.vim'
Plug 'mhinz/vim-mix-format'

" Other Utils
Plug 'godlygeek/tabular'
Plug '/usr/local/opt/fzf' " fzf, installed using homebrew

call plug#end()

" Line numbers
set number
set nuw=4

" Save everything on buffer switch...
set autowriteall
" ...and on focus lost
:au FocusLost * silent! wa

" Theme
set background=dark
color gruvbox
let g:lightline = {}
let g:lightline.colorscheme = 'gruvbox'
set noshowmode " We don't need to display -- MODE -- anymore

" Use Spaces instead of tabs
set tabstop=2     " Size of a hard tabstop (ts).
set shiftwidth=2  " Size of an indentation (sw).
set expandtab     " Always uses spaces instead of tab characters (et).
set softtabstop=0 " Number of spaces a <Tab> counts for. When 0, featuer is off (sts).
set autoindent    " Copy indent from current line when starting a new line.
set smarttab      " Inserts blanks on a <Tab> key (as per sw, ts and sts).

" IndentLine configuration
let g:indentLine_color_term = 8
let g:indentLine_char = '·'
let g:indentLine_leadingSpaceChar = '·'
let g:indentLine_leadingSpaceEnabled = 1

" Gitgutter
let g:gitgutter_override_sign_column_highlight = 0
highlight SignColumn ctermbg=black
highlight GitGutterAdd ctermfg=green ctermbg=black
highlight GitGutterChange ctermfg=yellow ctermbg=black
highlight GitGutterDelete ctermfg=red ctermbg=black
highlight GitGutterChangeDelete ctermfg=yellow ctermbg=black

" Split:
" navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

